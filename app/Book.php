<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 * @property-read int id
 * @property string title
 * @property string author
 * @property string created_at
 * @property string updated_at
 */
class Book extends Model
{
    protected $table = 'books';

    protected $fillable = ['title', 'author'];
}
