<?php


namespace App\Services;


use App\Book;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BookService
 * @package App\Services
 */
class BookService
{
    /**
     * @return Book[]|Collection
     */
    public function getAllBooks()
    {
        return Book::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getBookById($id)
    {
        return Book::find($id);
    }

    /**
     * @param array $data
     * @return Book
     */
    public function createBook(array $data)
    {
        $book = new Book();
        $book->fill($data);
        $book->save();
        return $book;
    }

    /**
     * @param int $id
     */
    public function deleteBookById($id)
    {
        Book::destroy($id);
    }
}
