<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BooksController extends Controller
{
    /**
     * @var BookService
     */
    private $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the bookings.
     *
     * @return Response
     */
    public function index()
    {
        $data['books'] = $this->bookService->getAllBooks();

        return view('books', $data);
    }

    /**
     * Store a newly created book in database.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $bookData = [
            'title' => $request->get('title'),
            'author' => $request->get('author'),
        ];

        $this->bookService->createBook($bookData);
        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->bookService->deleteBookById($id);
        return redirect('/books');
    }
}
