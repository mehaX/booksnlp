@extends('layouts.app')

<?php
/**
 * @var \App\Book[]|\Illuminate\Database\Eloquent\Collection $books
 */
?>
@section('content')
    <div class="container mt-5">
        <form class="jumbotron" method="post">
            @csrf
            <div class="row mb-3">
                <label class="col-md-2 text-right">
                    Title
                </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="title">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 text-right">
                    Author
                </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="author">
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-4 offset-md-2">
                    <button class="btn btn-primary">Add</button>
                </div>
            </div>
        </form>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Author</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>

            <tbody>
            @foreach($books as $book)
                <tr>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->author }}</td>
                    <td>
                        <form action="{{ url('books', $book->id) }}" method="post">
                            @method('DELETE')
                            @csrf

                            <button onclick="return confirm('Are you sure you want to delete this book?')"
                                    class="btn btn-sm btn-secondary delete-book-js"><span class="fa fa-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
